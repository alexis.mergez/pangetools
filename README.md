# PanGeTools
```
PanGeTools - Pangenomic General tools - Version 1.10.12d
    This container wraps several tools used for working with pangenomes.
    It contains :
        - GFAffix        v0.2.0
        - smoothxg       v0.8.0
        - odgi           v0.9.0
        - vg             v1.63.1
        - panacus        v0.2.3
        - gfatools       v0.5
        - GFAvc          v0.4
        - GFAstats       v0.4.2
        - samtools       v1.19
        - bgzip          v1.19.1
        - wfmash         v0.10.5
        - seqwish        v0.7.11
        - minimap2       v2.26
        - vcfbub         v0.1.0
        - vcflib         v1.0.9
        - RTG            v3.12.1
        - Mash           v2.3
        - pansel         v1.0
        - GraphAligner   v1.0.19
        - Seqkit         v2.9.0
        - GraphicsMagick v1.3.38
    Use apptainer --app <toolname> <container_path> [command] to run the selected tool.
    Tool names are lowercase : gfaffix, smoothxg, odgi, vg, panacus, etc...
```
Apptainer container containing useful tools for pangenomics analysis. Versions of tools is reported in the definition file, in the help section.

# Install
To download the container : 
```
apptainer pull PanGeTools.sif oras://registry.forgemia.inra.fr/alexis.mergez/pangetools/pangetools:latest
```
To execute the container :
```
./PanGeTools.sif <command>
# or
apptainer run --app <cmd name> PanGeTools.sif <cmd args>
```

Add the following function to your bashrc to have easier commands : 
```
# Folder containing apptainer images (.sif)
apptainerpath="<path_to_apptainer_folder>"

# Function to easly run PanGeTools.
# The command is PanGeTools <tool name> <tool args>
PanGeTools() {
    apptainer run --app $1 $apptainerpath/PanGeTools.sif "${@:2}"
}
# Alias to update PanGeTools to latest.
alias PGTUp="apptainer build $apptainerpath/PanGeTools.sif oras://registry.forgemia.inra.fr/alexis.mergez/pangetools/pangetools:latest"
```