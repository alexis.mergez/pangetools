#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
GFAvc: GFA version converter.
Convert GFA from v1.1 to v1.0 (Convert walk to paths) and vice versa.

@author: alexis.mergez@inrae.fr
@version: 0.4.2
"""
import re
import argparse
import os
import gzip

version = "0.4.2"

## Argument parser
arg_parser = argparse.ArgumentParser(description='GFAvc: GFA version converter')
arg_parser.add_argument(
    "--gfa1",
    "-g",
    dest = "GFA1",
    help = "GFA 1.1 file. (Gzip or not)"
    )
arg_parser.add_argument(
    "--gfa",
    "-G",
    dest = "GFA",
    help = "GFA 1.0 file. (Gzip or not)"
    )
arg_parser.add_argument(
    "--outName",
    "-o",
    dest = "outName",
    required = True,
    help = "Output file name."
    )
arg_parser.add_argument(
    "--simplify",
    action="store_true",
    dest = "simplify",
    help = "Removes range from path names, usefull for compatibility with Pan1c workflow"
    )
arg_parser.add_argument(
    "--index",
    "-i",
    dest = "index",
    help = "TSV containing start stop for each path in order to convert to walk (optional)"
    )
arg_parser.add_argument(
    '--version',
    '-v',
    action="store_true",
    dest = "version",
    help = "Show version"
)
args = arg_parser.parse_args()

#% Returning version 
if args.version:
    print(version)
    os._exit(0)     

#% Parsing index
if args.index is not None:
    with open(args.index, 'r') as handle:
        file = [line.rstrip() for line in handle.readlines()]

    index = {}
    for line in file:
        split = line.split("\t")
        index[split[0]] = [split[1], split[2]]

else : index = None

#% Conversion functions
def gfa11_to_gfa10(gfa1_file = args.GFA1):
    #% Reading GFA
    # If not gzipped :
    if gfa1_file[-2:] != "gz" :
        with open(gfa1_file, 'r') as file:
            gfa = [line.rstrip() for line in file.readlines()]

    # If gzipped :
    else :
        with gzip.open(gfa1_file, 'r') as file:
            gfa = [line.decode().rstrip() for line in file.readlines()]

    #% Changing version number in header
    assert gfa[0].split('\t')[1] == "VN:Z:1.1"
    _ = gfa[0].split('\t')
    _[1] = "VN:Z:1.0"
    gfa[0] = "\t".join(_)

    # Used to store paths are order them at the end
    paths_dict = {}
    gfa_out = []

    #% Iterating in reverse to put paths at the end.
    for lineID in range(len(gfa)):
        if gfa[lineID][0] == "S" :
            gfa_out.append('\t'.join(gfa[lineID].split('\t')[:3]))

        elif gfa[lineID][0] == "W" :

            curLine = gfa[lineID].split('\t')
            
            # Transforming '>..>..<..>..' to ['>..', '>..', '<..', '>..']
            curWalk = re.findall(r'>\w+|<\w+', curLine[-1])

            # Converting ['>..', '>..', '<..', '>..'] to '..+,..+,..-,..+'
            path = [f'{elem[1:]}{(elem[0] == ">")*"+"+(elem[0] == "<")*"-"}' for elem in curWalk]

            if not args.simplify :
                name = f"{'#'.join(curLine[1:5])}-{curLine[5]}"
            else :
                name = f"{'#'.join(curLine[1:4])}"

            newLine = ['P', name, ','.join(path), '*']

            paths_dict[name] = '\t'.join(newLine)
            #gfa.append('\t'.join(newLine))

        #% Moving path lines to the end
        elif gfa[lineID][0] == "P" :

            curLine = gfa[lineID]
            name = curLine.split('\t')[1]
            paths_dict[name] = '\t'.join(curLine)
            #gfa.append(curLine)

        else :
            gfa_out.append(gfa[lineID])

    for name in sorted(paths_dict.keys()):
        gfa_out.append(paths_dict[name])

    return gfa_out

def gfa10_to_gfa11(gfa_file = args.GFA, index = index):

    #% Reading GFA
    # If not gzipped :
    if gfa_file[-2:] != "gz" :
        with open(gfa_file, 'r') as file:
            gfa = [line.rstrip() for line in file.readlines()]

    # If gzipped :
    else :
        with gzip.open(gfa_file, 'r') as file:
            gfa = [line.decode().rstrip() for line in file.readlines()]

    sign = {"+": ">", "-": "<"}
    samples = []

    # Used to store paths are order them at the end
    paths_dict = {}
    gfa_out = []

    #% Iterating in reverse to put walks at the end.
    for lineID in range(len(gfa)):

        if gfa[lineID][0] == "P" :

            curLine = gfa[lineID].split('\t')

            # Converting '..+,..+,..-,..+' to ['>..', '>..', '<..', '>..']
            walk = [
                f"{sign[elem[-1]]}{elem[:-1]}" 
                for elem in curLine[-2].split(',')
            ]

            splittedID = curLine[1].split(":")
            if len(splittedID) == 2:
                # Range info is available
                ID = splittedID[0].split("#")
                RANGE = splittedID[1].split("-")
            elif len(splittedID) == 1 and index is not None:
                ID = splittedID[0].split("#")
                RANGE = index[splittedID[0]]
            else :
                ID = splittedID[0].split("#")
                RANGE = 2*["*"]
                    
            newLine = ['W'] + ID + RANGE + [f"{''.join(walk)}"]
            samples.append(ID[0])

            paths_dict[splittedID[0]] = '\t'.join(newLine)
            #gfa.append('\t'.join(newLine))

        #% Moving walk lines to the end
        elif gfa[lineID][0] == "W" :

            curLine = gfa[lineID]
            name = '#'.join(curLine.split('\t')[1:4])
            paths_dict[name] = '\t'.join(curLine)
            #gfa.append(curLine)

        else :
            gfa_out.append(gfa[lineID])


    for name in sorted(paths_dict.keys()):
        gfa_out.append(paths_dict[name])

    samples = list(set(samples))

    #% Changing version number in header
    assert gfa_out[0].split('\t')[1] == "VN:Z:1.0"
    _ = gfa_out[0].split('\t')
    _[1] = "VN:Z:1.1"
    _.append(f"RS:Z:{' '.join(samples)}")
    gfa_out[0] = "\t".join(_)

    return gfa_out

#% Selecting the conversion direction
if args.GFA is None and args.GFA1 is not None:
    print("[GFAvc] Converting from GFA 1.1 to GFA 1.0 ...")
    gfa = gfa11_to_gfa10()

elif args.GFA is not None and args.GFA1 is None:
    print("[GFAvc] Converting from GFA 1.0 to GFA 1.1 ...")
    gfa = gfa10_to_gfa11()

else:
    print("[GFAvc] Unable to convert !")
    os._exit(1)

#% Exporting
gfa[-1] = f"{gfa[-1]}\n"
with open(args.outName, "w") as file:
    file.write("\n".join(gfa))