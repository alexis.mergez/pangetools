#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Anchors2Path
Give anchor nodes IDs with their positions along the given path.

@author: alexis.mergez@inrae.fr
@version: 0.1.2
"""

import argparse
import os
import numpy as np
import time
import pandas as pd
import gzip

version = "0.1.2"

## Argument parser
arg_parser = argparse.ArgumentParser(description='Anchors2Path')
arg_parser.add_argument(
    "--gfa",
    "-g",
    dest = "gfa",
    required = True,
    help = "GFA file"
    )  
arg_parser.add_argument(
    "--output",
    "-o",
    dest = "output",
    required = True,
    help = "Output name"
    )  
arg_parser.add_argument(
    '--version',
    '-v',
    action="store_true",
    dest = "version",
    help = "Show version"
)
arg_parser.add_argument(
    '--progress',
    '-P',
    action="store_true",
    dest = "progress",
    help = "Show progress to stdout"
)
arg_parser.add_argument(
    '--pathname',
    '-r',
    dest = "pathname",
    required = True,
    help = "Pathname"
)
args = arg_parser.parse_args()

# Printing version and exiting if required
if args.version:
    print(version)
    os._exit(0)

print("Version:\t", version)

# Timing the script
start_time = time.time()

## Reading the gfa into a list
# If not gzipped :
if args.gfa[-2:] != "gz" :
    with open(args.gfa, 'r') as file:
        gfaLines = file.readlines()

# If gzipped :
else :
    with gzip.open(args.gfa, 'r') as file:
        gfaLines = [line.decode() for line in file.readlines()]

# Progress message
if args.progress: print(f"[Anchors2Path] Parsing gfa file...")

# Initializing dictionnaries
Anchors = {}
## {<NODE_ID>: (path_start, path_end)}
nodes_length = {}
path_nodes = {}
## {<path_id>: <nodes_list>}

for line in gfaLines[1:]:

    # Skipping comment lines
    if line[0] == "#":
        lineType = "#"

    # Reading 3 first columns of the current line
    else :
        lineType, uid, value = line[:-1].split('\t')[:3]

    if lineType == "S": # Segments = Nodes
        nodes_length[int(uid)] = len(value)

    elif lineType == "P": # Paths
        
        path_nodes[uid] = [int(k[:-1]) for k in value.split(",")]

if args.progress:
    print(f"[Anchors2Path] Parsed in {round(time.time() - start_time, 2)}s")

# Getting the list of anchor nodes
node_path_count = {}
## {<NODE_ID>: <Number of path traversing this node>}
# Computing number of path traversing each nodes
for path_id, node_list in path_nodes.items():

    nodes_counts = np.unique(node_list, return_counts=True)
    print(f"[Anchors2Path] Counting nodes in {path_id}")
    for i in range(len(nodes_counts[0])):
        node_id, count = nodes_counts[0][i], nodes_counts[1][i]
        
        if count == 1 :
            try :
                node_path_count[node_id] += 1
            except :
                node_path_count[node_id] = 1

# Searching anchors
n_path = len(list(path_nodes.keys()))

for node_id, count in node_path_count.items():
    if count == n_path :
        Anchors[node_id] = []

# Computing path position for each node of the path of interest
current_pos = 0
ordered_anchors = []
for node_id in path_nodes[args.pathname]:
    _end = current_pos + nodes_length[node_id]

    # Trying to add anchors path position if it is an anchor
    try :
        Anchors[int(node_id)].append( (current_pos, _end) )
        
        # Keeping track of order of appearance of anchors
        ordered_anchors.append(int(node_id))
    except:
        pass

    current_pos = _end

# Filtering anchors based on 

# Transforming data into a table
ID, START, END = [], [], []
for node_id, positions in Anchors.items():
    for start, end in positions:
        ID.append(node_id)
        START.append(start)
        END.append(end)

df = pd.DataFrame(data = {
    "NODE_ID": ID,
    "START": START,
    "END": END
})

df.to_csv(args.output, sep="\t", index = False)

